defmodule Auction.PageControllerTest do
    use Auction.ConnCase
    alias Auction.{Item, Repo}
  
    test "POST /bid (confirmation)", %{conn: conn} do
        [%{description: "Chicago Bullet Speed Skate (Size 7)", price: 59.0, closing_date: Ecto.Date.cast!("2018-01-24")}]
        |> Enum.map(fn item_data -> Item.changeset(%Item{}, item_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
        item = Repo.get_by!(Item, description: "Chicago Bullet Speed Skate (Size 7)")
        conn = post conn, "/bid", %{offer: [id: item.id, price: "62"]}
        conn1 = get conn, redirected_to(conn)
        assert html_response(conn1, 200) =~ ~r/offer taken into account/
    end
    test "POST /bid (rejection)", %{conn: conn} do
        [%{description: "Chicago Bullet Speed Skate (Size 7)", price: 59.0, closing_date: Ecto.Date.cast!("2018-01-24")}]
        |> Enum.map(fn item_data -> Item.changeset(%Item{}, item_data) end)
        |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
        item = Repo.get_by!(Item, description: "Chicago Bullet Speed Skate (Size 7)")
        conn = post conn, "/bid", %{offer: [id: item.id, price: "50"]}
        conn1 = get conn, redirected_to(conn)
        assert html_response(conn1, 200) =~ ~r/offer has not been taken into account/
    end

end


