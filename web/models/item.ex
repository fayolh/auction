defmodule Auction.Item do
  use Auction.Web, :model

  schema "items" do
    field :description, :string
    field :price, :float
    field :closing_date, :date
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:description, :price, :closing_date])
    |> validate_required([:description, :price, :closing_date])
  end

  def bid_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:price])
    |> validate_required([:price])
    |> validate_bid
  end
  def validate_bid(changeset) do
    old_price = changeset.data.price
    new_price = changeset.changes.price
    if new_price > old_price do
      changeset
    else
      changeset |> add_error(:price, "new bid lower than actual price")
    end
  end
end
