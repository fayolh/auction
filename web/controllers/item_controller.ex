defmodule Auction.ItemController do
  use Auction.Web, :controller
  alias Auction.{Repo, Item}
  alias Ecto.{Changeset}

  def index(conn, _params) do
    items = Repo.all(Item)
    render conn, "index.html", items: items
  end

  def show(conn, %{"id" => id}) do
    item = Repo.get!(Item, id)
    render conn, "show.html", item: item
  end
  
  def bid(conn, %{"offer" => offer_params} = params) do
    item = Repo.get!(Item, offer_params["id"])
    changeset = Item.bid_changeset(item, %{price:  Float.parse(offer_params["price"])|>elem(0)}) 
    if changeset.valid? do
      Repo.update!(changeset)
      conn 
      |> put_flash(:info, "offer taken into account")
      |> redirect(to: item_path(conn, :index))
    else
      conn 
      |> put_flash(:error, "offer has not been taken into account")
      |> redirect(to: item_path(conn, :index))
    end
  end
end
