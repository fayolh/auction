defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Auction.{Repo, Item}
  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)    
    %{}
  end
  scenario_starting_state fn state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Auction.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Auction.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Auction.Repo)
    Hound.end_session
  end 

  given_ ~r/^the following open auctions$/, 
  fn state, %{table_data: table} ->
    table
    |> Enum.map(fn item -> %{item | closing_date: item.closing_date |> Ecto.Date.cast!} end)
    |> Enum.map(fn item -> Item.changeset(%Item{}, item) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to make an offer of "(?<price>[^"]+)" for "(?<item>[^"]+)"$/,
  fn state, %{price: price, item: item} ->

    {:ok, state |> Map.put(:price, price) |> Map.put(:item, item)}
  end

  and_ ~r/^I open the online auction web page$/, fn state ->
    navigate_to "/items"
    {:ok, state}
  end

  and_ ~r/^I select my desired item$/, fn state ->
    row = find_all_elements(:tag, "tr") |> Enum.find(&(visible_text(&1)=~state[:item]))
    find_within_element(row, :class, "btn-xs") |> click
    {:ok, state}
  end

  and_ ~r/^I enter my offer$/, fn state ->
    fill_field({:id, "price"}, state[:price])
    {:ok, state}
  end

  when_ ~r/^I summit my bid$/, fn state ->
    find_element(:class, "btn-primary") |> click
    {:ok, state}
  end

  then_ ~r/^I should receive a confirmation message$/, fn state ->
    assert visible_in_page? ~r/offer taken into account/ 
    {:ok, state}
  end

  then_ ~r/^I should receive a rejection message$/, fn state ->
    assert visible_in_page? ~r/offer has not been taken into account/
    {:ok, state}
  end
end
