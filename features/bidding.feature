Feature: Bidding
  As a customer
  Such that I acquire an item
  I want to place a bid

  Scenario: Bidding via online auction system (with confirmation)
    Given the following open auctions
          | description                              | price  | closing_date |
          | Chicago Bullet Speed Skate (Size 7)      | 59.00  | 2018-01-24	 |
          | Riedell Dart Derby Skates (Size 8)       | 106.00 | 2018-01-19   |
          | Roller Derby Brand Blade Skate (Size 7)  | 112.00 | 2018-01-12   |
    And I want to make an offer of "62.00" for "Chicago Bullet Speed Skate (Size 7)"
    And I open the online auction web page
    And I select my desired item
    And I enter my offer
    When I summit my bid
    Then I should receive a confirmation message
  
  Scenario: Bidding via online auction system (with rejection)
    Given the following open auctions
          | description                              | price  | closing_date |
          | Chicago Bullet Speed Skate (Size 7)      | 59.00  | 2018-01-24	 |
          | Riedell Dart Derby Skates (Size 8)       | 106.00 | 2018-01-19   |
          | Roller Derby Brand Blade Skate (Size 7)  | 112.00 | 2018-01-12   |
    And I want to make an offer of "100.00" for "Riedell Dart Derby Skates (Size 8)"
    And I open the online auction web page
    And I select my desired item
    And I enter my offer
    When I summit my bid
    Then I should receive a rejection message